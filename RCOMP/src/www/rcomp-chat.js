
// global variables
var nextMsg;
var mArea, messageBox, hints, wallBox, idxBox; // defined only after the document is loaded

function loadAndStart() {
    mArea = document.getElementById("messages");
    messageBox = document.getElementById("message");
    hints = document.getElementById("hints");
    wallBox = document.getElementById("wall");
    idxBox = document.getElementById("idx");
    wallDeleteBox = document.getElementById("nameWallDelete");
    nextMsg = 0;
    setTimeout(getNextMessage, 1000);
}

function getNextMessage() {
    var request = new XMLHttpRequest();

    request.onload = function () {
        if (nextMsg === 0) {
            mArea.value = "";
            mArea.style.color = "blue";
        }
        mArea.value = mArea.value + this.responseText + "\r\n";
        mArea.innerHTML = mArea.innerHTML +"<p>" +this.responseText +"</p>";
        mArea.scrollTop = mArea.scrollHeight; // scroll the textarea to make last lines visible
        nextMsg = nextMsg + 1;
        setTimeout(getNextMessage, 100);
    };

    request.onerror = function () {
        nextMsg = 0;
        mArea.value = "Server not responding.";
        mArea.style.color = "red";
        setTimeout(getNextMessage, 1000);
    };

    request.ontimeout = function () {
        nextMsg = 0;
        mArea.value = "Server not responding.";
        mArea.style.color = "red";
        setTimeout(getNextMessage, 100);
    };


    request.open("GET", "/walls/" + wallBox.value + "/" + nextMsg, true);
    if (nextMsg === 0)
        request.timeout = 1000;
    // Message 0 is a server's greeting, it should always exist
    // no timeout, for following messages, the server responds only when the requested
    // message number exists
    request.send();
}

function postMessage() {
    hints.innerHTML = "";
    if (wallBox.value === "") {
        hints.innerHTML = "Settle a wallBox before sending.";
        return;
    }
    
    if (messageBox.value === "") {
        hints.innerHTML = "Not sending empty message.";
        return;
    }
    var POSTrequest = new XMLHttpRequest();
    wallBox.disabled = true;

    POSTrequest.open("POST", "/walls/" + wallBox.value, true);
    POSTrequest.timeout = 5000;
    POSTrequest.send(messageBox.value);
}

function deleteMessage() {
    hints.innerHTML = "";
    if (idxBox.value === "") {
        hints.innerHTML = "Settle the index of Message.";
        return;
    }
    
    var POSTrequest = new XMLHttpRequest();

    POSTrequest.open("DELETE", "/walls/" + wallBox.value+"/" +idxBox.value, true);
    POSTrequest.timeout = 5000;
    POSTrequest.send(idxBox.value);
    
}
 
function AlterWall(){
    if (wallBox.value === "") {
        hints.innerHTML = "Cannot Alter Wall because is Empty";
        return;
    }
    
    wallBox.disabled = false;
    location.reload();
        
}

function deleteWall() {
    hints.innerHTML = "";
    if (wallDeleteBox.value === "") {
        hints.innerHTML = "Settle the name of Wall.";
        return;
    }
    
    var POSTrequest = new XMLHttpRequest();

    POSTrequest.open("DELETE", "/walls/" + wallBox.value, true);
    POSTrequest.timeout = 5000;
    POSTrequest.send(wallDeleteBox.value);
    location.reload();
    
}
