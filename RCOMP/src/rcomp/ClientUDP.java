/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author Wimy Carvalho
 */
public class ClientUDP {

    static InetAddress serverIP;

    public static void main(String args[]) throws Exception {
        byte[] data = new byte[300];
        String sentence;
        serverIP = InetAddress.getByName("255.255.255.255");

        DatagramSocket sock = new DatagramSocket();
        sock.setBroadcast(true);
        DatagramPacket udpPacket = new DatagramPacket(data, data.length, serverIP, 32032);

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("Enter Wall Name (\"exit\" to quit): ");
            sentence = in.readLine();
            if (sentence.compareTo("exit") == 0) {
                break;
            }
            udpPacket.setData(sentence.getBytes());
            udpPacket.setLength(sentence.length());
            sock.send(udpPacket);
            System.out.print("Enter your sentence: ");
            sentence = in.readLine();
            udpPacket.setData(sentence.getBytes());
            udpPacket.setLength(sentence.length());
            sock.send(udpPacket);
            udpPacket.setData(data);
            udpPacket.setLength(data.length);
        }
        sock.close();
    }
}
