package rcomp;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 *
 * @author ANDRE MOREIRA (asc@isep.ipp.pt)
 */
public class HttpServerChat {

    static private final String BASE_FOLDER = "src\\www";
    static private ServerSocket sock;
    static private Set<Wall> WALL_LIST = new HashSet<>();

    public static void main(String args[]) throws Exception {
        Socket cliSock;

        if (args.length != 1) {
            System.out.println("Local port number required at the command line.");
            System.exit(1);
        }
        try {
            sock = new ServerSocket(Integer.parseInt(args[0]));
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + args[0]);
            
            System.exit(1);
        }
        
        ConnectUdpHttp connect = new ConnectUdpHttp(new DatagramSocket(32032));
        connect.start();
        
        System.out.println("Server ready, listening on port number " + args[0]);
        addMsg("sa", "HTTP Chat Server is ready ...");
        while (true) {
            cliSock = sock.accept();
            HttpChatRequest req = new HttpChatRequest(cliSock, BASE_FOLDER);
            req.start();
        }
    }

    // MESSAGES ARE ACCESSED BY THREADS - LOCKING REQUIRED
    private static int nextMsgNum = 0;
    private static final ArrayList<String> MSG_LIST = new ArrayList<>();

    public static List<Message> getMsg(String nomeWall) {
        Wall parede = new Wall(nomeWall);

        for (Wall w : WALL_LIST) {
            if (w.equals(parede)) {
                synchronized (w.getMessageList()) {
                    return w.getMessageList();
                }
            }

        }
        return null;
    }

    public static void addMsg(String wall, String msg) {
        Wall idx = new Wall(wall);

        if (!WALL_LIST.contains(wall)) {
            WALL_LIST.add(idx);
        }

        for (Wall w : WALL_LIST) {
            if (w.getWallName().equalsIgnoreCase(wall)) {
                synchronized (w.getMessageList()) {
                    w.getMessageList().add(new Message(msg));
                    w.getMessageList().notifyAll();
                }
                break;
            }
        }

    }

    public static void rmMsg(String wall, int idxSms) {

        for (Wall w : WALL_LIST) {
            if (w.getWallName().equalsIgnoreCase(wall)) {
                synchronized (w.getMessageList()) {
                    w.getMessageList().remove(idxSms);
                    w.getMessageList().notifyAll();
                }
                break;
            }
        }

    }

    /**
     *
     * @param nomeParede
     */
    public static boolean esperarEvento(String nomeParede) {
        boolean flag = false;

        for (Wall next : WALL_LIST) {
            if (next.getWallName().equalsIgnoreCase(nomeParede)) {
                try {

                    System.out.println("Estou a espera");
                    synchronized (next.getMessageList()) {
                        next.getMessageList().wait();
                        System.out.println("Já não Estou a espera");
                        return true;
                    }
                } catch (InterruptedException ex) {
                    System.out.println("Erro na espera de um evento.");
                }
            }
        }
        return flag;
    }

    public static String transformarString(List<Message> msg) {
        String s = "";
        int cont = 0;
        for (Message m : msg) {
            s += "#" + cont + "-" + m.getMessage() + "\n";
            cont++;
        }
        return s;
    }

    public static void rmWall(String wall) {

        for (Wall w : WALL_LIST) {
            if (w.getWallName().equalsIgnoreCase(wall)) {
                WALL_LIST.remove(w);
            }
        }
    }

}
