package rcomp;

import java.io.*;
import java.net.*;
import java.util.List;

/**
 *
 * @author ANDRE MOREIRA (asc@isep.ipp.pt)
 */
public class HttpChatRequest extends Thread {

    String baseFolder;
    Socket sock;
    DataInputStream inS;
    DataOutputStream outS;
    private static int index = 0;

    public HttpChatRequest(Socket s, String f) {
        baseFolder = f;
        sock = s;
    }

    @Override
    public void run() {
        try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }
        try {
            HTTPmessage request = new HTTPmessage(inS);
            HTTPmessage response = new HTTPmessage();
            response.setResponseStatus("200 Ok");

            System.out.println("Method : " + request.getMethod());

            if (request.getMethod().equals("GET")) {
                if (request.getURI().startsWith("/walls/")) {
                    String s[] = request.getURI().split("/");
                    System.out.println("GET Wall:" + s[2]);
                    if (HttpServerChat.esperarEvento(s[2])) {
                        List<Message> newMsg = HttpServerChat.getMsg(s[2]);
                        // if msgNum doesn't yet exist, the getMsg() method waits
                        // until it does. So the HTTP request was received, but the
                        // HTTP response is sent only when there's a message
                        response.setContentFromString(HttpServerChat.transformarString(newMsg), "text/plain");
                        index++;
                    }
                } else { // NOT GET /messages/ , THEN IT MUST BE A FILE
                    String fullname = baseFolder + "/";
                    if (request.getURI().equals("/")) {
                        fullname = fullname + "index.html";
                    } else {
                        fullname = fullname + request.getURI();
                    }
                    if (!response.setContentFromFile(fullname)) {
                        response.setContentFromString(
                                "<html><body><h1>404 File not found</h1></body></html>",
                                "text/html");
                        response.setResponseStatus("404 Not Found");
                    }
                }
            } else if (request.getMethod().equals("POST")) {
                if (request.getURI().startsWith("/walls/")) {
                    String s[] = request.getURI().split("/");
                    System.out.println("Post Wall:" + s[2]);
                    HttpServerChat.addMsg(s[2], request.getContentAsString());
                    response.setResponseStatus("200 Ok");
                } else {
                    response.setContentFromString(
                            "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
                            "text/html");
                    response.setResponseStatus("405 Method Not Allowed");
                }
            } else {
                if ((request.getMethod().equals("DELETE")) && request.getURI().startsWith("/walls/")) {
                    String s[] = request.getURI().split("/");

                    switch (s.length) {
                        //Se tiver 4 trata se do delete msg de uma wall -> ""/walls/wallName/msg
                        case 4: {
                            HttpServerChat.rmMsg(s[2], Integer.parseInt(s[3]));
                            response.setResponseStatus("200 Ok");
                            break;
                        }
                        //Se tiver 3 trata-se do delete wall -> ""/walls/wallName
                        case 3: {
                            HttpServerChat.rmWall(s[2]);
                            response.setResponseStatus("200 Ok");
                            break;
                        }

                        //Erro se tiver outro tamanho
                        default:
                            response.setContentFromString(
                                    "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
                                    "text/html");
                            response.setResponseStatus("405 Method Not Allowed");
                            break;
                    }
                }
            }
            response.send(outS); // SEND THE HTTP RESPONSE
        } catch (IOException ex) {
            System.out.println("Thread I/O error on request/response");
        }

        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
    }
}
