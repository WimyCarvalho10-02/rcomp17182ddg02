/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Wimy Carvalho - 1161297
 */
public class Wall {
 private String wallName;
    private ArrayList<Message> messageList;

    public Wall(String wallName) {
        this.wallName = wallName;
        this.messageList = new ArrayList<>();
        this.messageList.add(new Message("Lets Talk !!!"));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Wall other = (Wall) obj;
        if (!Objects.equals(this.wallName, other.wallName)) {
            return false;
        }
        return true;
    }

    public ArrayList<Message> getMessageList() {
        return messageList;
    }

    public String getWallName() {
        return wallName;
    }

}
