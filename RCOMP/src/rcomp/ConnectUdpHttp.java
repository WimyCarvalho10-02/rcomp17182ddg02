package rcomp;

import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 *
 * @author Wimy Carvalho
 */
public class ConnectUdpHttp extends Thread {

    private final int port = 32032;
    static DatagramSocket sock;
    byte[] data = new byte[300];
    String wall;
    String sentence;

    public ConnectUdpHttp (DatagramSocket sock) {
        this.sock = sock;
    }

    @Override
    public void run() {
        DatagramPacket udpPacket = new DatagramPacket(data, data.length);
        wall = "";
        sentence = "";
        while (true) {

            try {
                sock.receive(udpPacket);
            } catch (IOException ex) {
                System.out.println("Erro ao receber o packet\n");
            }
            wall = new String(udpPacket.getData(), 0, udpPacket.getLength());
            udpPacket.setData(data);
            udpPacket.setLength(data.length);
            System.out.println(wall);
            try {
                sock.receive(udpPacket);
            } catch (IOException ex) {
                System.out.println("Erro ao receber o packet\n");
            }
            sentence = new String(udpPacket.getData(), 0, udpPacket.getLength());
            udpPacket.setData(data);
            udpPacket.setLength(data.length);
            System.out.println(sentence);
            HttpServerChat.addMsg(wall, sentence);
            System.out.println("Received reply: " + sentence + " from wall: " + wall);
        }
    }

}

